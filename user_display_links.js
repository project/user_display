
/**
 * Cache for already queried users.
 */
Drupal.userDisplayLinksStack = [];

/**
 * Current hovered DOM element.
 */
Drupal.userDisplayLinksHoverNode = null;

/**
 * Attach user menu to user images.
 */
Drupal.userDisplayLinksAttach = function() {
  // Setup synchronous requests for user menus.
  $.ajaxSetup({
    url: '/js/user_display_links/links',
    async: false
  });
  $('.user-links:not(.user-links-processed)').each(function() {
    $(this)
      .addClass('user-links-processed')
      .click(function() {
          Drupal.userDisplayLinksLoad(this);
          Drupal.userDisplayLinksHoverNode = this;
          Drupal.userDisplayLinksHover(this, true);
          return false;
        }
      )
      // Avoid mouseover clashes by removing title attributes from images and
      // links.
      .find('img, a').attr('title', '');
  });
}

/**
 * Query user menu for a given user.
 */
Drupal.userDisplayLinksLoad = function(node) {
  var uid = node.className.replace(/.*user-(\d+).*/g, '$1');
  if (Drupal.userDisplayLinksStack[uid]) {
    return;
  }
  $('body').append('<div id="user-links-'+ uid +'" class="user-links-menu"></div>');
  $.getJSON('/js/user_display_links/links/'+ uid, function(response) {
    Drupal.userDisplayLinksStack[uid] = response.data;
    $('#user-links-'+ uid).html(response.data).hide()
      .hover(function() {
        },
        function() {
          Drupal.userDisplayLinksHover(node, false);
          Drupal.userDisplayLinksHoverNode = null;
        }
      );
  });
}

/**
 * Ensure current hovered DOM element; apply UI effects.
 */
Drupal.userDisplayLinksHover = function(node, hover) {
  if (Drupal.userDisplayLinksHoverNode != node) {
    return;
  }
  var uid = node.className.replace(/.*user-(\d+).*/g, '$1');
  var $menu = $('#user-links-'+ uid);
  if (hover) {
    var $node = $(node);
    var offset = $node.offset();
    var left = offset.left + (($node.width() - $menu.width()) / 2);
    var top = offset.top + (($node.height() - $menu.height()) / 2);
    $menu.css({position: 'absolute', left: left, top: top}).fadeIn('fast');
  }
  else {
  	$menu.fadeOut('fast');
  }
}

if (Drupal.jsEnabled) {
  $(document).ready(Drupal.userDisplayLinksAttach);
}
