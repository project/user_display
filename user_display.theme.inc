<?php

/**
 * @file
 * Overrides for the standard user theme functions.
 */

/**
 * @ingroup themeable
 * @{
 */

/**
 * Format a username.
 *
 * This backwards compatible implementation of theme_username() has additional
 * support to show the user's picture (provided that it is available in the
 * $account).
 *
 * @param $account
 *   The user account to format, usually returned from user_load().
 * @param $class
 *   A user display class, leave empty to output default username.
 * @param $style
 *   A user display style to force, for example when invoked via
 *   phptemplate_user_picture().
 * @return
 *   A string containing an HTML link to the user's page if the passed object
 *   suggests that this is a site user. Otherwise, only the username is returned.
 */
function phptemplate_username($account, $class = NULL, $style = NULL) {
  if (isset($class)) {
    $styles = user_display_styles();
    if (!is_numeric($class)) {
      $classes = variable_get('user_display_classes', array());
      $style = isset($classes[$class]) ? $styles[$classes[$class]] : NULL;
    }
    else {
      $style = isset($styles[$class]) ? $styles[$class] : NULL;
    }
  }
  if ($account->uid && $account->name && isset($style)) {
    $content = '';
    $classes = array('display');
    foreach ($style['elements'] as $element) {
      if (function_exists($element[0])) {
        // Invoke element callback.
        $element = call_user_func_array($element[0], array_merge(array($account, $style), isset($element[1]) ? $element[1] : array()));
        if (!empty($element) && is_array($element)) {
          // Add rendered content to output.
          $content .= $element['content'];
          if (!empty($element['class'])) {
            // Add defined element classes to user-display container.
            $classes = array_merge($classes, $element['class']);
          }
        }
      }
    }
    // Skip potential duplicate classes.
    $classes = array_unique($classes);
    $output = '<div class="user-'. implode(' user-', $classes) .'">';
    $output .= $content;
    $output .= '</div>';
  }
  else {
    // Fallback on core theme function.
    $output = theme_username($account);
  }

  return $output;
}

/**
 * Display a user picture.
 *
 * This backwards compatible implementation of theme_user_picture() has
 * additional support to use imagecache presets for user pictures.
 *
 * @param $account
 *   A user object with the picture property set.
 * @param $namespace
 *   A valid imagecache preset.
 */
function phptemplate_user_picture($account, $class = NULL, $preset = NULL) {
  if (variable_get('user_pictures', 0)) {
    if (!isset($preset) && isset($class)) {
      $styles = user_display_styles();
      if (!is_numeric($class)) {
        $classes = variable_get('user_display_classes', array());
        $style = isset($classes[$class]) ? $styles[$classes[$class]] : NULL;
      }
      else {
        $style = isset($styles[$class]) ? $styles[$class] : NULL;
      }
      if (isset($style)) {
        return theme('username', $account, NULL, $style);
      }
    }
  
    // Fall back to default presets for user style and other pages.
    if (!isset($preset)) {
      $presets = user_display_imagecache_presets();
      if (arg(0) == 'user' && is_numeric(arg(1))) {
        $preset = isset($presets['default_profile']) ? 'default_profile' : USER_DISPLAY_DEFAULT_PICTURE;
      }
      else {
        $preset = isset($presets['default']) ? 'default' : USER_DISPLAY_DEFAULT_PICTURE;
      }
    }
  
    if ($account->picture && file_exists($account->picture)) {
      $file = $account->picture;
    }
    else if (variable_get('user_picture_default', '')) {
      $file = variable_get('user_picture_default', '');
    }

    if (!empty($file)) {
      $alt = t("@user's picture", array('@user' => $account->name ? $account->name : variable_get('anonymous', t('Anonymous'))));
      // Render an user picture generated via ImageCache.
      if (module_exists('imagecache') && is_string($preset)) {
        $picture = theme('imagecache', $preset, $file, $alt, $alt);
      }
      // Render user picture like Drupal core if no valid class has been given.
      else if (is_numeric($preset) && $preset == USER_DISPLAY_DEFAULT_PICTURE) {
        $picture = theme('image', $file, $alt, $alt, '', FALSE);
      }
      // Fall back on username if picture has been disabled.
      else {
        return theme_username($account);
      }
      // Add user profile link to the user picture.
      if (!empty($account->uid) && user_access('access user profiles')) {
        $picture = l($picture, "user/$account->uid", array('title' => t('View user profile.')), NULL, NULL, FALSE, TRUE);
      }

      return "<div class=\"user-picture\">". $picture .'</div>';
    }
  }
}

/**
 * Display a list of users.
 *
 * This backwards compatible implementation of theme_user_list() has additional
 * support to show user pictures.
 *
 * @param $users
 *   An array with user objects. Should contain at least the name and uid.
 * @return
 *   description
 */
function phptemplate_user_list($users, $title = NULL, $class = NULL) {
  if (!empty($users)) {
    foreach ($users as $user) {
      $items[] = theme('username', $user, $class);
    }
  }
  return theme('item_list', $items, $title);
}

/**
 * @} End of "ingroup themeable".
 */
