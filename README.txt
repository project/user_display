
-- SUMMARY --

User Display aims at providing a centralized configuration and extendable API
to consistently display user names and pictures (avatars) across a site.  By 
supporting User Display's API, third party modules give site owners complete
control over how user names and pictures should be shown.  For example, users
in the "Who's online" block may be displayed with their picture only, while
users in comments could be shown with their name, picture, and other elements.

Another feature of User Display is the Action Links submodule, an AHAH driven,
customizable context menu for user links, allowing users to execute various
user related actions, for example write a private message or add a user to the
buddy list.

For a full description visit the project page:
  http://drupal.org/project/user_display
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/user_display


-- BACKGROUND --

Users are visually represented by "display styles". A style can consist of
various configurable elements that we call "decorators", for example user name,
picture, online status, role, etc.  By leveraging User Display's API, third
party modules are able to extend the available elements, for example add a
country flag based on user profile data.

To be able to display customized users, third party modules need to support 
User Display by making "display classes" available. Display classes identify
all locations where users are shown, and provide the link between User Display
and the third party module.


-- REQUIREMENTS --

* ImageCache v2.x


-- INSTALLATION --

* Make sure that your theme does not contain the following functions (by
  commenting them out or removing them completely):

  - phptemplate_username()
  - phptemplate_user_picture()
  - phptemplate_user_list()

  You should also make sure that neither of these functions is overridden by
  your theme (i.e. it exists with 'nameofyourtheme_' prefix instead of
  'phptemplate_').

* Install as usual, see http://drupal.org/node/70151 for further information.

* To enable User Display API support in nodes and comments rendered by Drupal
  core, copy the file user_display_phptemplate.patch to the root directory
  of your Drupal installation and execute the following shell command there:

  patch -b -p0 < user_display_phptemplate.patch


-- CONFIGURATION --

* As long as the User Display UI module is not completed, either enable Help
  module or see user_display_ui.txt to learn how to setup user display styles
  and classes.

* Configure user display styles in Administer >> User management >>
  >> User Display.


-- TROUBLESHOOTING --

* If you have set up a user display style and class but Drupal outputs the
  default user name or picture, you should double-check that all PHP files in
  your theme do not output theme('username') or theme('user_picture') directly.


-- DEVELOPER INFO --

You are the author of a module and want to give back site owners control over
how to display user names and pictures? Great! You can support User Display by
adding a "display class" (much like a CSS class, although CSS isn't used here)
to all relevant theme('username') and theme('user_picture') calls.  For
example, a module which displays a status block with user names or pictures
might add the class 'mymodule_block_status' as additional argument like this:

$output .= theme('username', $account, 'mymodule_block_status');

By implementing hook_user_display(), your module may then assign a title and 
description for each available class, which in turn can be used by site admins
to control the visual representation.  And the best thing is: because the core
theme functions don't use the added parameter, your code is still fully
backwards compatible! No nasty module_exists() hacks required to support User
Display.


-- CONTACT --

Developed by:
* Daniel F. Kudwien (sun) - dev@unleashedmind.com
* Stefan M. Kudwien (smk-ka) - dev@unleashedmind.com

This project has been sponsored by:
* UNLEASHED MIND
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.unleashedmind.com for more information.

